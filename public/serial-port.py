import serial
import requests

API_KEY = 'dummy'
API_ENDPOINT = 'http://127.0.0.1:8000/api/get-barcode'
ser = serial.Serial();
ser.port = 'COM4'
ser.open();
print("connected to: " + ser.portstr)
print('Please start scanning with the Scanner...')



while True:
    data = ser.read(ser.inWaiting()).decode('utf-8')
    if data:
        POST_DATA = {
        			 'event': 'barcodeEvent' ,
        			 'data': data,
        			 }
        response = requests.post(url = API_ENDPOINT, data = POST_DATA) 
        print('Barcode Updated');

ser.close();

