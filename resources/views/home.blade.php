<html>
<head>
    <title>Inspection</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style>
        #main{
            margin-top: 100px;
        }

        .wl-card{
            width: 100%;
            height: 400px;
            /*box-shadow: 0px 0px 5px rgba(0,0,0, 0.2);*/
            margin-top: 10%;
        }

        th{
            text-align: center;
        }

        tr>td:nth-child(3){
            width: 4%;
        }

        tr>td:nth-child(2){
            width: 10%;
        }

        tr>td:nth-child(4){
            width: 8%;
        }

        tr>td:nth-child(1){
            width: 10%;
        }

        tr>td{
            text-align: center;
        }

        .wl-card>h3{
            margin-top: 20px;
            margin-bottom: 20px;

        }

        select:disabled,input[type="text"]:disabled{
            background-color: gainsboro !important;
        }

        input[type='checkbox']{
            display: block;
        }


    </style>

<div id="loader" class="loader"></div>
<section id="header">
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a class="active-menu" href="#">Inspection</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>
</head>
<body>

<section id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="wl-card">
                    <h3>ZELL A Inspection</h3>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Material</th>
                                <th>DOFF No</th>
                                <th>Spindle</th>
                                <th>Total Weight (kg)</th>
                                <th>Tare Weight(kg)</th>
                                <th>Material Weight(kg)</th>
                                <th>Machine</th>
                                <th>Status</th>
                                <th>Reason</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



    <div id="reweightPopup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                    <h4 class="modal-title">Reweight Confirmation</h4>
                </div>
                <div class="modal-body">
                    {{--<div class="container">--}}
                        <div class="row">
                            <form action="#" id="reweight-form" class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="reweight-status">Reweight Status </label>
                                            <select name="reweight-status" id="reweight-status">
                                                <option value="no">No</option>
                                                <option value="yes">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="reweight-reason">Reweight Reason </label>
                                            <select name="reweight-reason" id="reweight-reason" disabled>
                                                <option value="Splice Portion">Splice Portion</option>
                                                <option value="dopes">Dopes</option>
                                                <option value="loose winding">Loose Winding</option>
                                                <option value="Shade Variation">Shade Variation</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="reweight">Reweight(Kg)</label>
                                            <input type="text" id="reweight" name="reweight" class="text-input" disabled required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary center-block" value="submit">
                                </div>
                            </form>

                        </div>
                    {{--</div>--}}
                </div>
            </div>

        </div>
    </div>


    <div id="responsePopup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Inspection Update Status</h4>
                </div>
                <div class="modal-body">
                    <h4 id="response"  style="margin-bottom: 20px;"></h4>
                    <input type="button" class="btn btn-primary center-block" style="width: 100px" onClick="window.location.reload()" value="Done">
                </div>
            </div>

        </div>
    </div>


<?php $reason = \App\NcrMaster::all(); ?>

</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
{{--<script src="{{url('js/simulate.js')}}"></script>--}}



<script>


    $(document).ready(function(){

        var reason = {!! json_encode($reason) !!};

        var select =$( $.parseHTML('<select name="reason" id="reason"></select>'));

        console.log(select.html());

        $.each(reason, function(k, v){
            console.log(v.tamil_reason);
           select.append('<option value="'+v.reason+'">'+v.tamil_reason+'</option>');
        });

        console.log(select);



        $('#reweight-status').on('change', function(){
           if($(this).val() == 'yes'){

               $('#reweight-reason, #reweight').prop('disabled', false);

           }else{

               $('#reweight-reason, #reweight').prop('disabled', true);

           }
        });

        function  showBarcodeData(data){

            data = JSON.parse(data);
            wl = data.data;
            //console.log(data.status, data.data)
            if(data.status){
                $('tbody').empty();
              //  console.log(jQuery.type(data.data));
                console.log(select.outerHTML)
                $.each(data.data, function(key, wl){
                    console.log(key, wl);
                    if(wl.inspection_status == 0 && wl.packed_status != 1 && wl.active_status == 1 && wl.weight_status == 1){
                        $('tbody').append(
                            '<tr>'+
                            '<td>'+wl.material+'</td>'+
                            '<td>'+wl.doff_no+'</td>'+
                            '<td>'+wl.spindle+'</td>'+
                            '<td>'+wl.total_weight+'</td>'+
                            '<td>'+wl.tare_weight+'</td>'+
                            '<td>'+wl.material_weight+'</td>'+
                            '<td>'+wl.machine+'</td>'+
                            '<td><select name="status" id="status" data-id='+wl.id+'>' +
                            '<option value="ok">Ok</option>' +
                            '<option value="notok">Not Ok</option>'+
                            '</select></td>'+
                            '<td><select name="reason" id="reason" disabled>'+select.html()+'</select></td>'+
                            '<td><input type="submit" class="btn btn-primary" id="submit-inspection" value="submit"/></td>'+
                            '</tr>'
                        );
                    }else if(wl.weight_status == 0){
                        alert('Cannot show Not ok spindle in Inspection')
                    }else{
                        alert('The Spindle is either packed or Inspected Already');
                    }

                });
            }else{
                alert('No data found for this QR Code');

            }
        }


        var socket = io('http://127.0.0.1:3000');

        socket.on('barcode-channel:barcodeEvent', function(data){

            showBarcodeData(data);
        });



        $(document).on('change','#status' ,function(){

            console.log($(this).val());
            if($(this).val() == 'notok'){
                $('#reason').prop('disabled', false);

                // openDropdown('reason')

            }else{
                $('#reason').prop('disabled', true);
                // $('#reason').click();
            }
        });

        $(document).on('change', '#reason',function(){
            $(this).blur();
        });




        $(document).on('click', '#submit-inspection',function(){
            var tr = $(this).parent().parent();
            var status = tr.find('#status').val();
            var id = tr.find('#status').data('id');
            var reason = 'none';
            var reweight = 0;

            if(status == "notok"){
                var reason = tr.find('#reason').find(":selected").val();
                var dataString = 'status='+status+'&id='+id+'&reason='+reason+'&reweight=0';
                console.log(reason);
                submitForm(dataString);
            }else{
                $('#reweightPopup').modal('show');
            }


        });

        $('#reweight-form').on('submit', function(e){
            e.preventDefault();
            // console.log($(this).serialize());
            var id = $(document).find('#status').data('id');

            var reweightStatus = $('#reweight-status').val();

            if(reweightStatus == "yes"){
                var dataString = 'status=ok'+'&reason='+$('#reweight-reason').val()+'&reweight='+$('#reweight').val()+'&id='+id;

            }else{
                var dataString = 'status=ok&reason=none&reweight=0'+'&id='+id;

            }

            submitForm(dataString);
        });



        function submitForm(dataString){

            console.log(dataString);
            $.ajax({
                type: "POST",
                url: "/update-weightlog",
                data: dataString,
                beforeSend: function() {},
                success: function(data, status, xhr) {
                    data = JSON.parse(data)
                    if(xhr.status == 200){
                        // alert('Updated Successfully');
                        if(data.status){
                            $('#reweightPopup').modal('hide');
                            $('#response').text(data.response);
                            $('#responsePopup').modal('show');
                        }else{
                            $('#reweightPopup').modal('hide');
                            $('#response').text(data.response);
                            $('#responsePopup').modal('show');
                        }
                        // location.reload();
                    }
                },
                error: function(xhr, status, error) {
                    if (xhr.status == 422) {

                        alert(xhr.responseJSON.errors)

                    }
                },
            })
        }


        $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

    });


    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.keyboard =  false;

</script>

</body>
</html>
