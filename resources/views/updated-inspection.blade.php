<html>
<head>
    <title>Updated Inspection</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style>
        #main{
            margin-top: 100px;
        }

        .wl-card{
            width: 100%;
            height: 400px;
            /*box-shadow: 0px 0px 5px rgba(0,0,0, 0.2);*/
            margin-top: 10%;
        }

        th{
            text-align: center;
        }

        tr>td:nth-child(3){
            width: 4%;
        }

        tr>td:nth-child(2){
            width: 10%;
        }

        tr>td:nth-child(4){
            width: 8%;
        }

        tr>td:nth-child(1){
            width: 10%;
        }

        tr>td{
            text-align: center;
        }

        .wl-card>h3{
            margin-top: 20px;
            margin-bottom: 20px;

        }

        select:disabled{
            background-color: gainsboro !important;
        }

    </style>

    <div id="loader" class="loader"></div>
    <section id="header">
        <header>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="#">Inspection</a></li>
                            <li><a class="active-menu" href="/updated-inspection">Updated Inspection</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

    </section>
</head>
<body>

<section id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="wl-card">
                    <h3>Updated Inspection</h3>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Material</th>
                            <th>DOFF No</th>
                            <th>Spindle</th>
                            <th>Total Weight (kg)</th>
                            <th>Tare Weight(kg)</th>
                            <th>Material Weight(kg)</th>
                            <th>Machine</th>
                            <th>Status</th>
                            <th>Reason</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>



<script>


    $(document).ready(function(){


        function  showBarcodeData(data){

            data = JSON.parse(data);

            console.log(data.status, data.data)
            if(data.status){
                $('tbody').empty();
                $.each(data.data, function(key, wl){

                    if(wl.inspection_status == 1 && wl.packed_status != 1){
                        $('tbody').append(
                            '<tr>'+
                            '<td>'+wl.material+'</td>'+
                            '<td>'+wl.doff_no+'</td>'+
                            '<td>'+wl.spindle+'</td>'+
                            '<td>'+wl.total_weight+'</td>'+
                            '<td>'+wl.tare_weight+'</td>'+
                            '<td>'+wl.material_weight+'</td>'+
                            '<td>'+wl.machine+'</td>'+
                            '<td><select name="status" id="status">' +
                            '<option value="ok">Ok</option>' +
                            '<option value="notok">Not Ok</option>'+
                            '</select></td>'+
                            '<td><select name="reason" id="reason" disabled>' +
                            '<option value="dopes">Dopes</option>'+
                            '<option value="dots">White Dots</option>'+
                            '</select></td>'+
                            '<td><input type="submit" class="btn btn-primary" value="submit"/></td>'+
                            '</tr>'
                        );
                    }

                });

            }else{
                alert('No data found for this QR Code');

            }
        }

        var socket = io('http://127.0.0.1:3000');

        socket.on('barcode-channel:barcodeEvent', function(data){

            showBarcodeData(data);
        });



        $(document).on('change','#status' ,function(){

            console.log($(this).val());
            if($(this).val() == 'notok'){
                $('#reason').prop('disabled', false);
                $(document).find('#reason').trigger('mousedown');
            }else{
                $('#reason').prop('disabled', true);
                // $('#reason').click();
            }
        });
    });

</script>

</body>
</html>
