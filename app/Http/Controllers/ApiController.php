<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;


use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getBarcodeDetails(Request $request){



        $apiKey = $request['api-key'];



        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/get-barcode-weight-log', [
            'form_params' => [
                'unique_id' => $request['data'],
                'api_key' => 'scpl-inspection-003',
            ]
        ]);


        //return $response->getBody()->getContents();


        $data = [
            'event' => $request['event'],
            'data' =>  $response->getBody()->getContents(),
        ];


         Redis::publish('barcode-channel', json_encode($data));
    }


    public function updateWeightLog(Request $request){
        $request->validate([
            'id' => 'required',
            'status' => 'required',
        ]);


        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/update-inspection-weight-log', [
            'form_params' => [
                'data' => $request->all(),
                'api_key' => 'scpl-inspection-003',
            ]
        ]);




        return $response->getBody()->getContents();


    }
}
